//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CardsByAccount
    {
        public int ID { get; set; }
        public int CardID { get; set; }
        public int AccountID { get; set; }
        public System.DateTime CreatedOn { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Card Card { get; set; }
    }
}
