﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public partial class Customer
    {
        public string CustomerTypeDescription
        {
            set
            {
                //Nothing at all
            }
            get
            {
                return this.CustomerType.Description;
            }
        }
    }
}
