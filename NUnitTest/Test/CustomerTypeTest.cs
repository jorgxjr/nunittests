﻿using DataAccess.CustomerTypes;
using Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest.Test
{
    [TestFixture]
    class CustomerTypeTest
    {
        [TestCase]
        public void CreateCustomerTypeTest()
        {
            var repository = new Mock<ICustomerTypeRepository>();
            CustomerType customerType = new CustomerType();
            repository.Setup(r => r.CreateCustomerType(customerType)).Returns(true);
            Assert.That(repository.Object.CreateCustomerType(customerType), Is.True);
        }
        [TestCase]
        public void UpdateCustomerTypeTest()
        {
            var repository = new Mock<ICustomerTypeRepository>();
            CustomerType customerType = new CustomerType();
            repository.Setup(r => r.UpdateCustomerType(customerType)).Returns(true);
            Assert.That(repository.Object.UpdateCustomerType(customerType), Is.True);
        }
        [TestCase]
        public void DeleteCustomerTypeTest()
        {
            var repository = new Mock<ICustomerTypeRepository>();
            int id = 1;
            repository.Setup(r => r.DeleteCustomerType(id)).Returns(true);
            Assert.That(repository.Object.DeleteCustomerType(id), Is.True);
        }
    }
}
