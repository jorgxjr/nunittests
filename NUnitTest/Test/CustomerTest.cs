﻿using DataAccess.Customers;
using Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest.Test
{
    [TestFixture]
    class CustomerTest
    {
        [TestCase]
        public void CreateCustomerTest()
        {
            var repository = new Mock<ICustomerRepository>();
            Customer customer = new Customer();
            repository.Setup(r => r.CreateCustomer(customer)).Returns(true);
            Assert.That(repository.Object.CreateCustomer(customer), Is.True);
        }
        [TestCase]
        public void UpdateCustomerTest()
        {
            var repository = new Mock<ICustomerRepository>();
            Customer customer = new Customer();
            repository.Setup(r => r.UpdateCustomer(customer)).Returns(true);
            Assert.That(repository.Object.UpdateCustomer(customer), Is.True);
        }
        [TestCase]
        public void DeleteCustomerTest()
        {
            var repository = new Mock<ICustomerRepository>();
            int id = 1;
            repository.Setup(r => r.DeleteCustomer(id)).Returns(true);
            Assert.That(repository.Object.DeleteCustomer(id), Is.True);
        }
    }
}
