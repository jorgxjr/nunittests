﻿using BusinessLogic.Customers;
using BusinessLogic.CustomerTypes;
using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UPCBank
{
    public partial class FrmCustomerMaintenance : Form
    {
        //Create customer type service
        ICustomerTypeService customerTypeService =
            new CustomerTypeService();

        //Create customer service
        ICustomerService customerService = 
            new CustomerService();
        public FrmCustomerMaintenance()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            //Read customer fields
            Customer objCustomer = new Customer();
            objCustomer.Address = txtAddress.Text;
            objCustomer.CustomerTypeID =
                Convert.ToInt32(cbCustomerTypes.SelectedValue);
            objCustomer.DateOfBirth = dtpDoB.Value;
            objCustomer.DocumentNumber = txtDocumentNumber.Text;
            if(rbMale.Checked)
            {
                objCustomer.Gender = "Male";
            }
            else
            {
                objCustomer.Gender = "Female";
            }
            objCustomer.Name = txtName.Text;
            objCustomer.RequiresCard = chbRequiresCard.Checked;

            customerService.CreateCustomer(objCustomer);
            bool flag = customerService.CreateCustomer(objCustomer);
            if (flag)
            {
                MessageBox.Show("Customer created!", "Success");
            }
            else
            {
                MessageBox.Show("Ooops", "Error!!");
            }

        }

        private void FrmCustomerMaintenance_Load(object sender, EventArgs e)
        {
            //Get customer types
            List<CustomerType> customerTypes =
                customerTypeService.GetCustomerTypes();

            //Assign list to drop down list
            cbCustomerTypes.DataSource = customerTypes;
            cbCustomerTypes.SelectedIndex = 0;

            //Load customers
            this.LoadCustomers();
        }

        private void LoadCustomers()
        {
            List<Customer> customers =
                customerService.GetCustomers();
            //foreach(var customer in customers)
            //{
            //    MessageBox.Show(customer.Name);
            //}
            dgvCustomers.DataSource = customers;
        }

        private void dgvCustomers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {   
            if(dgvCustomers.
                Columns[e.ColumnIndex].
                HeaderText.
                ToLower() == "delete")
            {
                if (MessageBox.Show
                    ("Are you sure you want to delete this record?",
                    "Confirm",
                    MessageBoxButtons.YesNo)
                    == DialogResult.Yes)
                {
                    //int customerId =
                    //    Convert.ToInt32(dgvCustomers.
                    //    Rows[e.RowIndex].
                    //    Cells[0].
                    //    Value);
                    Customer objCustomer = 
                        (Customer)dgvCustomers.
                        Rows[e.RowIndex].
                        DataBoundItem;
                    this.customerService.
                        DeleteCustomer(objCustomer.ID);
                    this.LoadCustomers();
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
