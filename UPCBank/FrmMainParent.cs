﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UPCBank
{
    public partial class FrmMainParent : Form
    {
        //Singleton implementaion for customer maintenance screen
        FrmCustomerMaintenance frmCustomerMaintenance;
        public FrmCustomerMaintenance MyFrmCustomerMaintenance
        {
            get
            {
                if(frmCustomerMaintenance == null ||
                    frmCustomerMaintenance.IsDisposed)
                {
                    frmCustomerMaintenance = 
                        new FrmCustomerMaintenance();
                    frmCustomerMaintenance.MdiParent = this;
                }
                return frmCustomerMaintenance;
            }
        }

        public FrmMainParent()
        {
            InitializeComponent();
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.MyFrmCustomerMaintenance.Show();
        }
    }
}
