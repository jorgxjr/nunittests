﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DATransactions
{
    public interface ITransactionRepository
    {
        void CreateTransaction(Transaction objTransaction);
        void UpdateTransaction(Transaction objTransaction);
        List<Transaction> GetTransactions();
        List<Transaction> GetTransactions(decimal amount);
        List<Transaction> GetTransactions(DateTime datetime);
        List<Transaction> GetTransactions(string currency);
    }
}
