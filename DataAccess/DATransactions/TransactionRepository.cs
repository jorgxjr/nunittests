﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data.Entity;

namespace DataAccess.DATransactions
{
    public class TransactionRepository : ITransactionRepository
    {
        public void CreateTransaction(Transaction objTransaction)
        {
            using(var dataContext = new BankUPCEntitiesContext())
            {
                dataContext.Transactions.Add(objTransaction);
                dataContext.SaveChanges();
            }
        }

        public List<Transaction> GetTransactions()
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                var tnts = from t in dataContext.Transactions
                           select t;
                return tnts.ToList();
            }
        }
        public List<Transaction> GetTransactions(string currency)
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                var tnts = from t in dataContext.Transactions
                           where t.Currency.ToLower().Equals(currency.ToLower())
                           select t;
                return tnts.ToList();
            }
        }

        public List<Transaction> GetTransactions(DateTime datetime)
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                var tnts = from t in dataContext.Transactions
                           where t.CreatedOn > DateTime.Now.Date.AddDays(-7)
                           select t;
                return tnts.ToList();
            }
        }

        public List<Transaction> GetTransactions(decimal amount)
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                var tnts = from t in dataContext.Transactions
                           where t.Amount > amount
                           select t;
                return tnts.ToList();
            }
        }

        public void UpdateTransaction(Transaction objTransaction)
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                //Method 1
                //dataContext.Transactions.Attach(objTransaction);
                //dataContext.Entry(objTransaction).State = EntityState.Modified;
                //dataContext.SaveChanges();

                //Method 2
                //Get object from database
                var trs = from t in dataContext.Transactions
                          where t.ID == objTransaction.ID
                          select t;
                Transaction transaction = trs.FirstOrDefault();
                //Update field by field
                transaction.Amount = objTransaction.Amount;
                transaction.Commission = objTransaction.Commission;
                //transaction.Currency = objTransaction.Currency;
                transaction.DestinationID = objTransaction.DestinationID;
                transaction.SourceID = objTransaction.SourceID;
                transaction.Status = objTransaction.Status;
                dataContext.SaveChanges();
            }
        }
    }
}
