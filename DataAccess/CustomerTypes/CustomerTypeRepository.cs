﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace DataAccess.CustomerTypes
{
    public class CustomerTypeRepository :
        ICustomerTypeRepository
    {
        public bool CreateCustomerType(CustomerType objCustomerType)
        {
            bool flag = false;
            try
            {
                using (var dataContext = new BankUPCEntitiesContext())
                {
                    dataContext.CustomerTypes.Add(objCustomerType);
                    dataContext.SaveChanges();
                }
                flag = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            return flag;
        }

        public bool DeleteCustomerType(int id)
        {
            bool flag = false;
            try
            {
                using (var dataContext =
               new BankUPCEntitiesContext())
                {
                    var cust = from c in dataContext.CustomerTypes
                               where c.ID == id
                               select c;
                    CustomerType objCustomerType = cust.FirstOrDefault();
                    dataContext.CustomerTypes.Remove(objCustomerType);
                    dataContext.SaveChanges();
                }
                flag = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            return flag;
        }

        public CustomerType GetCustomerType(int id)
        {
            throw new NotImplementedException();
        }

        public List<CustomerType> GetCustomerTypes()
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                var custs = from c in dataContext.CustomerTypes
                            select c;
                return custs.ToList();
            }
        }

        public bool UpdateCustomerType(CustomerType objCustomerType)
        {
            bool flag = false;
            try
            {
                using (var dataContext = new BankUPCEntitiesContext())
                {
                    var cust = from t in dataContext.CustomerTypes
                               where t.ID == objCustomerType.ID
                               select t;
                    CustomerType customerType = cust.FirstOrDefault();
                    customerType.Description = objCustomerType.Description;
                    dataContext.SaveChanges();
                }
                flag = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            return flag;
        }
    }
}
