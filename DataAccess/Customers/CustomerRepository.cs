﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace DataAccess.Customers
{
    public class CustomerRepository : ICustomerRepository
    {
        public bool CreateCustomer(Customer objCustomer)
        {
            bool flag = false;
            try
            {
                using (var dataContext = new BankUPCEntitiesContext())
                {
                    dataContext.Customers.Add(objCustomer);
                    dataContext.SaveChanges();
                }
                flag = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            return flag;
        }

        

        public bool DeleteCustomer(int customerID)
        {
            bool flag = false;
            try
            {
                using (var dataContext =
                new BankUPCEntitiesContext())
                {
                    var cust = from c in dataContext.Customers
                               where c.ID == customerID
                               select c;
                    Customer objCustomer = cust.FirstOrDefault();
                    dataContext.Customers.Remove(objCustomer);
                    dataContext.SaveChanges();
                }
                flag = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            return flag;
        }
        public Customer GetCustomer(int customerID)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetCustomers()
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                //LINQ
                //return dataContext.Customers.ToList();
                var custs = from c in dataContext.Customers.
                            Include("CustomerType")
                                //where c.Name == "Alguien"
                            select c;
                return custs.ToList();
            }
        }

        public List<Customer> GetCustomers(string customerType)
        {
            using (var dataContext = new BankUPCEntitiesContext())
            {
                //LINQ
                var custs = from c in dataContext.Customers.
                                        Include("CustomerType")
                            where c.CustomerType.Description.
                                        ToLower().
                                        Equals(customerType.ToLower())
                            select c;
                return custs.ToList();
            }
        }

        public bool UpdateCustomer(Customer objCustomer)
        {
            bool flag = false;
            try
            {
                using (var dataContext = new BankUPCEntitiesContext())
                {
                    var cust = from t in dataContext.Customers
                              where t.ID == objCustomer.ID
                              select t;
                    Customer customer = cust.FirstOrDefault();
                    customer.CustomerTypeID = objCustomer.CustomerTypeID;
                    customer.Name = objCustomer.Name;
                    customer.DocumentNumber = objCustomer.DocumentNumber;
                    customer.Address = objCustomer.Address;
                    customer.DateOfBirth = objCustomer.DateOfBirth;
                    customer.RequiresCard = objCustomer.RequiresCard;
                    customer.Gender = objCustomer.Gender;
                    dataContext.SaveChanges();
                }
                flag = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
            return flag;
        }
    }
}
