﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Customers
{
    public interface ICustomerRepository
    {
        bool CreateCustomer(Customer objCustomer);
        bool UpdateCustomer(Customer objCustomer);
        bool DeleteCustomer(int customerID);
        List<Customer> GetCustomers();
        List<Customer> GetCustomers(string customerType);
        Customer GetCustomer(int customerID);
        
    }
}
