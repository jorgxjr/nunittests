﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using DataAccess.Customers;

namespace BusinessLogic.Customers
{
    public class CustomerService : ICustomerService
    {
        ICustomerRepository repository =
                new CustomerRepository();
        public bool CreateCustomer(Customer objCustomer)
        {
            return repository.CreateCustomer(objCustomer);
        }

        

        public bool DeleteCustomer(int customerID)
        {
            return repository.DeleteCustomer(customerID);
        }

        public Customer GetCustomer(int customerID)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetCustomers()
        {
            return repository.GetCustomers();
        }

        public bool UpdateCustomer(Customer objCustomer)
        {
            return repository.UpdateCustomer(objCustomer);
        }
    }
}
