﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Customers
{
    public interface ICustomerService
    {
        bool CreateCustomer(Customer objCustomer);
        bool UpdateCustomer(Customer objCustomer);
        bool DeleteCustomer(int customerID);
        List<Customer> GetCustomers();
        Customer GetCustomer(int customerID);
        
    }
}
