﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.CustomerTypes
{
    public interface ICustomerTypeService
    {
        bool CreateCustomerType(CustomerType objCustomerType);
        bool UpdateCustomerType(CustomerType objCustomerType);
        bool DeleteCustomerType(int id);
        CustomerType GetCustomerType(int id);
        List<CustomerType> GetCustomerTypes();
    }
}
