﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using DataAccess.CustomerTypes;

namespace BusinessLogic.CustomerTypes
{
    public class CustomerTypeService : ICustomerTypeService
    {
        ICustomerTypeRepository repository =
                new CustomerTypeRepository();
        public bool CreateCustomerType(CustomerType objCustomerType)
        {
            return repository.CreateCustomerType(objCustomerType);
        }

        public bool DeleteCustomerType(int id)
        {
            return repository.DeleteCustomerType(id);
        }

        public CustomerType GetCustomerType(int id)
        {
            throw new NotImplementedException();
        }

        public List<CustomerType> GetCustomerTypes()
        {
            return repository.GetCustomerTypes();
        }

        public bool UpdateCustomerType(CustomerType objCustomerType)
        {
            return repository.UpdateCustomerType(objCustomerType);
        }
    }
}
